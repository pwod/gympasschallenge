package br.com.gympass.dto;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Comparator;

public class PilotoDto implements Comparator<PilotoDto> {

	private LocalTime hora;
	private Integer idPiloto;
	private String piloto;
	private Integer numVolta;
	private Duration tempoVolta;
	private Double velocidadeMedia;

	public PilotoDto() {

	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public Integer getIdPoloto() {
		return idPiloto;
	}

	public void setIdPiloto(Integer idPoloto) {
		this.idPiloto = idPoloto;
	}

	public String getPiloto() {
		return piloto;
	}

	public void setPiloto(String piloto) {
		this.piloto = piloto;
	}

	public Integer getNumVolta() {
		return numVolta;
	}

	public void setNumVolta(Integer numVolta) {
		this.numVolta = numVolta;
	}

	public Duration getTempoVolta() {
		return tempoVolta;
	}

	public void setTempoVolta(Duration tempoVolta) {
		this.tempoVolta = tempoVolta;
	}

	public Double getVelocidadeMedia() {
		return velocidadeMedia;
	}

	public void setVelocidadeMedia(Double velocidadeMedia) {
		this.velocidadeMedia = velocidadeMedia;
	}

	@Override
	public String toString() {
		return "PilotoDto [hora=" + hora + ", idPoloto=" + idPiloto + ", piloto=" + piloto + ", numVolta=" + numVolta
				+ ", tempoVolta=" + tempoVolta + ", velocidadeMedia=" + velocidadeMedia + "]";
	}

	@Override
	public int compare(PilotoDto o1, PilotoDto o2) {
		return o1.compare(o1, o2);
	}
	
	
}
