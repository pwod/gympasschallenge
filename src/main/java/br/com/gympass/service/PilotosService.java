package br.com.gympass.service;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import br.com.gympass.domain.Piloto;
import br.com.gympass.dto.PilotoDto;

/**
 * 
 * @author patrick.duarte
 *
 */
public class PilotosService {

	/**
	 * 
	 * @param pilotos
	 * @param listaFormadada
	 * @return retorna lista com todos os Pilotos convertidos de arquivo texto para um Objeto.
	 */
	public List<PilotoDto> incluiMaisUmPilotoNaLista(List<PilotoDto> pilotos, List<String> listaFormadada) {

		PilotoDto pilotoDto = new PilotoDto();

		pilotoDto.setHora(getLocalTime(listaFormadada.get(0)));
		pilotoDto.setIdPiloto(Integer.valueOf(listaFormadada.get(1)));
		pilotoDto.setPiloto(listaFormadada.get(2));
		pilotoDto.setNumVolta(Integer.valueOf(listaFormadada.get(3)));

		pilotoDto.setTempoVolta(getDuration(listaFormadada.get(4)));
		pilotoDto.setVelocidadeMedia(Double.valueOf(listaFormadada.get(5).replace(",", ".")));

		pilotos.add(pilotoDto);

		return pilotos;
	}

	/**
	 * 
	 * @param lista com todos pilotos
	 * @return retorna Lista com calculo de posi��o dos pilotos por ordem de chegada baseado no tempo.
	 */
	public List<Piloto> retornaListaComPosicoesDosPilotosPorOrdemDeChegada(List<PilotoDto> pilotos) {

		int contadorPosicaoDeChegadaPiloto = retiraPilotosDuplicadoseRetornaTamanhoDaLista(pilotos);

		List<Piloto> listaPilotos = new ArrayList<>();

		LocalTime tempo = pilotos.get(0).getHora();

		for (PilotoDto pilotoDto : pilotos) {

			if (listaPilotos.stream().noneMatch(obj -> obj.getCodigoPiloto().equals(pilotoDto.getIdPoloto()))
					&& pilotoDto.getNumVolta() == 3 && pilotoDto.getNumVolta() != 4) {
				Piloto piloto = new Piloto();
				piloto.setCodigoPiloto(pilotoDto.getIdPoloto());
				piloto.setNomePiloto(pilotoDto.getPiloto());
				piloto.setPosicaoChegada(contadorPosicaoDeChegadaPiloto);
				piloto.setQtdeVoltasCompletas(pilotoDto.getNumVolta());
				piloto.setTempoTotalProva(
						pilotoDto.getTempoVolta().multipliedBy(Long.valueOf(pilotoDto.getNumVolta())));

				listaPilotos.add(piloto);

				contadorPosicaoDeChegadaPiloto--;
			}

			if (tempo.compareTo(pilotoDto.getHora()) > 0 && pilotoDto.getNumVolta() == 4) {
				Piloto piloto = new Piloto();
				piloto.setCodigoPiloto(pilotoDto.getIdPoloto());
				piloto.setNomePiloto(pilotoDto.getPiloto());
				piloto.setPosicaoChegada(contadorPosicaoDeChegadaPiloto);
				piloto.setQtdeVoltasCompletas(pilotoDto.getNumVolta());
				piloto.setTempoTotalProva(
						pilotoDto.getTempoVolta().multipliedBy(Long.valueOf(pilotoDto.getNumVolta())));

				listaPilotos.add(piloto);

				contadorPosicaoDeChegadaPiloto--;

			}

		}

		ordenaListaPorPosicaoDeChegada(listaPilotos);

		return listaPilotos;

	}

	/**
	 * 
	 * @param listaPilotos
	 */
	private void ordenaListaPorPosicaoDeChegada(List<Piloto> listaPilotos) {
		Comparator<Piloto> ordernarPorPosicaoChegada = (s1, s2) -> {
			return s1.getPosicaoChegada().compareTo(s2.getPosicaoChegada());
		};

		listaPilotos.sort(ordernarPorPosicaoChegada);
	}

	/**
	 * 
	 * @param lista de pilotos
	 * @return retorna tamanho da lista de pilotos sem duplicados.
	 */
	private int retiraPilotosDuplicadoseRetornaTamanhoDaLista(List<PilotoDto> pilotos) {
		return pilotos.stream().collect(Collectors
				.toCollection(() -> new TreeSet<PilotoDto>((p1, p2) -> p1.getIdPoloto().compareTo(p2.getIdPoloto()))))
				.size();
	}

	/**
	 * 
	 * @param tempo
	 * @return localtime tempo.
	 */
	public LocalTime getLocalTime(String tempo) {
		return LocalTime.parse(tempo);
	}

	/**
	 * 
	 * @param duracao
	 * @return converte para Dura��o.
	 */
	public Duration getDuration(String duracao) {
		String[] valores = duracao.split(":");

		String valoresMilisegundos = valores[1];

		String[] valoresDosMilisegundos = valoresMilisegundos.split("[.]");

		Duration duration = Duration.ofMinutes(Long.parseLong(valores[0]));
		duration = duration.plusSeconds(Long.parseLong(valoresDosMilisegundos[0]));
		duration = duration.plusMillis(Long.parseLong(valoresDosMilisegundos[1]));
		return duration;

	}

	/**
	 * 
	 * @param listaFormadada
	 * @param linha
	 * @return formata as linhas do arquivo retirando espa�os ou tra�os e devolve uma lista limpa.
	 */
	public List<String> formataLinhasDoArquivoTexto(String linha) {
		List<String> listaFormadada = new ArrayList<>();

		
		String[] string = linha.split(" +");

		for (String texto : string) {

			if (!texto.isEmpty() && !texto.equals("") && !texto.equals("�")) {
				listaFormadada.add(texto);
			}
		}

		return listaFormadada;
	}

}
