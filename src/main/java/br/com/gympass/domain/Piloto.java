package br.com.gympass.domain;

import java.time.Duration;

/**
 * 
 * @author patrick.duarte
 *
 */
public class Piloto {
    private Integer posicaoChegada;
    private Integer codigoPiloto;
    private String nomePiloto;
    private Integer qtdeVoltasCompletas;
    private Duration tempoTotalProva;
    
	public Integer getPosicaoChegada() {
		return posicaoChegada;
	}
	public void setPosicaoChegada(Integer posicaoChegada) {
		this.posicaoChegada = posicaoChegada;
	}
	public Integer getCodigoPiloto() {
		return codigoPiloto;
	}
	public void setCodigoPiloto(Integer codigoPiloto) {
		this.codigoPiloto = codigoPiloto;
	}
	public String getNomePiloto() {
		return nomePiloto;
	}
	public void setNomePiloto(String nomePiloto) {
		this.nomePiloto = nomePiloto;
	}
	public Integer getQtdeVoltasCompletas() {
		return qtdeVoltasCompletas;
	}
	public void setQtdeVoltasCompletas(Integer qtdeVoltasCompletas) {
		this.qtdeVoltasCompletas = qtdeVoltasCompletas;
	}
	public Duration getTempoTotalProva() {
		return tempoTotalProva;
	}
	public void setTempoTotalProva(Duration tempoTotalProva) {
		this.tempoTotalProva = tempoTotalProva;
	}
	@Override
	public String toString() {
		return "Piloto [posicaoChegada=" + posicaoChegada + ", codigoPiloto=" + codigoPiloto + ", nomePiloto="
				+ nomePiloto + ", qtdeVoltasCompletas=" + qtdeVoltasCompletas + ", tempoTotalProva=" + tempoTotalProva
				+ "]";
	}
	
}
