package br.com.gympass;

import br.com.gympass.domain.Piloto;
import br.com.gympass.dto.PilotoDto;
import br.com.gympass.service.PilotosService;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author patrick.duarte
 *
 */
public class Main {
	public static void main(String[] args) throws Exception {
		PilotosService service = new PilotosService();
		List<PilotoDto> pilotos = new ArrayList<>();
		List<PilotoDto> listaComTodosPilotosVindaDoArquivoTexto = null;
		List<String> listaFormadadaSemEspacosOuTracos = null;

		Path caminho = Paths.get("src/main/resources/pilotos.txt");

		List<String> linhasLidasDoArquivo = Files.lines(caminho, StandardCharsets.UTF_8).collect(Collectors.toList());

		if (linhasLidasDoArquivo != null) {
			for (String linha : linhasLidasDoArquivo) {
				listaFormadadaSemEspacosOuTracos = service.formataLinhasDoArquivoTexto(linha);
				listaComTodosPilotosVindaDoArquivoTexto = service.incluiMaisUmPilotoNaLista(pilotos, listaFormadadaSemEspacosOuTracos);

			}
		} else {
			throw new Exception("lista vazia");
		}

		System.out.println("#################### SORT");

		Comparator<PilotoDto> comparador = (s1, s2) -> {
			return s1.getHora().compareTo(s2.getHora());
		};

		if (listaComTodosPilotosVindaDoArquivoTexto != null) {
			listaComTodosPilotosVindaDoArquivoTexto.sort(comparador.reversed());
		} else {
			throw new Exception("lista vazia");
		}

		List<Piloto> listaDePilotosComOrdenacaoPorPosicaoDeChegada = service
				.retornaListaComPosicoesDosPilotosPorOrdemDeChegada(listaComTodosPilotosVindaDoArquivoTexto);

		listaDePilotosComOrdenacaoPorPosicaoDeChegada.stream().forEach(System.out::println);

	}

}
